FROM microsoft/dotnet:sdk AS build
WORKDIR /src

# Copy solution/csproj and restore as distinct layers
COPY /*.sln ./

# Copy nuget package
#RUN mkdir -p nuget.packages
#COPY /NuGet.config ./
#COPY /nuget.packages/* nuget.packages/

RUN mkdir -p 50011210005

COPY /50011210005/*.csproj 50011210005/

RUN dotnet restore

# Copy everything else and build
COPY . ./
WORKDIR /src/50011210005
RUN dotnet publish -c Release -o /app

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime AS base
RUN ln -fs /usr/share/zoneinfo/Asia/Bangkok /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata

WORKDIR /app
COPY --from=build /app .
ENV ASPNETCORE_URLS=http://+:5000
EXPOSE 5000
#ENTRYPOINT ["dotnet", "50011210005.dll"]