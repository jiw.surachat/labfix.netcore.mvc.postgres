﻿using System;
using System.Collections.Generic;

namespace _50011210005.Models
{
    public partial class Region
    {
        public short RegionId { get; set; }
        public string RegionDescription { get; set; }
    }
}
