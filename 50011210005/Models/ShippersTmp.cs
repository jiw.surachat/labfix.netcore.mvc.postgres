﻿using System;
using System.Collections.Generic;

namespace _50011210005.Models
{
    public partial class ShippersTmp
    {
        public short ShipperId { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
    }
}
