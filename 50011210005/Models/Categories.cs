﻿using System;
using System.Collections.Generic;

namespace _50011210005.Models
{
    public partial class Categories
    {
        public short CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public byte[] Picture { get; set; }
    }
}
