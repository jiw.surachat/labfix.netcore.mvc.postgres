﻿using System;
using System.Collections.Generic;

namespace _50011210005.Models
{
    public partial class Territories
    {
        public string TerritoryId { get; set; }
        public string TerritoryDescription { get; set; }
        public short RegionId { get; set; }
    }
}
