﻿using System;
using System.Collections.Generic;

namespace _50011210005.Models
{
    public partial class OrderDetails
    {
        public short OrderId { get; set; }
        public short ProductId { get; set; }
        public float UnitPrice { get; set; }
        public short Quantity { get; set; }
        public float Discount { get; set; }
    }
}
