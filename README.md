### Dowload
```
db test : https://code.google.com/archive/p/northwindextended/downloads
pgAdmin (client) :  https://www.pgadmin.org/download/pgadmin-4-windows/
```
### docker-compose.pgsql.yml
```
version: '3'
services:
    postgres-db:
      container_name: postgres-db
      image: postgres:11.2
      restart: always
      ports:
        - 5432:5432
      environment:
        - POSTGRES_USER=postgres
        - POSTGRES_DB=northwind
        - POSTGRES_PASSWORD=1234
      volumes:
        - postgresdata:/var/lib/postgresql/data
volumes:
  postgresdata:

```

### Install Package
```
 $> cd 50011210005
 $> dotnet add . package Microsoft.AspNetCore.Mvc.TagHelpers
 $> dotnet add . package Microsoft.EntityFrameworkCore
 $> dotnet add . package NewtonSoft.Json
 $> dotnet add . package Npgsql.EntityFrameworkCore.PostgreSQL
 $> dotnet add . package Npgsql.EntityFrameworkCore.PostgreSQL.Design
 $> dotnet add . package Microsoft.EntityFrameworkCore.Tools.DotNet

```

### Create Directories Models
```
$> mkdir Models
```

### Create Models via EF
```
$> dotnet ef dbcontext scaffold "Host=192.168.99.100;Database=northwind;Username=postgres;Password=1234" Npgsql.EntityFrameworkCore.PostgreSQL -o Models -f

```

### Add connection string into "appsetings.json"
```
{
  "Logging": {
    "LogLevel": {
      "Default": "Warning"
    }
  },
  "AllowedHosts": "*",
  
  "ConnectionStrings" : {
      "PostgressConnection": "User ID=postgres;Password=1234;Host=192.168.99.100;Port=5432;Database=northwind;Pooling=true;"
   }

}

```

### EDIT "Startup.cs"
```
using _50011210005.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
```
```
public Startup(IConfiguration configuration){
    Configuration = configuration;
}

public IConfiguration Configuration {get;}
```
And
```
public void ConfigureServices(IServiceCollection services)
{
    services.AddMvc();

    //Add this
    var pgConnectionString = Configuration.GetConnectionString("PostgressConnection");
    services.AddDbContext<northwindContext>(options => options.UseNpgsql(pgConnectionString));
}
```
Add MapRoute
```
    routes.MapRoute(
        name:"manage",
        template:"{controller=Manage}/{action=Index}/{id?}"
    );
```

# Create Manage Customer

## Create "Controllers/ManageController.cs"
```
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using _50011210005.Models;
using System.Linq;
using System.Text;
using System;

namespace LabFix.NetcoreMVC.Controllers
{
    public class ManageController : Controller
    {
        private northwindContext _dbContext;
        public ManageController(northwindContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IActionResult Customer()
        {

            var cusAll = _dbContext.Customers.ToList();

            return View(cusAll);

        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        public string GenerateAutoId(int length)
        {
            const string valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        [HttpPost]
        public IActionResult Create(Customers customers)
        {
            if (ModelState.IsValid)
            {
                customers.CustomerId = GenerateAutoId(4);
                _dbContext.Customers.Add(customers);
                _dbContext.SaveChanges();

                return RedirectToAction("Customer");
            }

            return View(customers);
        }


        [HttpGet]
        public IActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            else
            {
                var data = _dbContext.Customers.FirstOrDefault(x => x.CustomerId == id);
                return View(data);
            }
        }

        [HttpPost]
        public IActionResult Edit(
            string id,
            [Bind("CustomerId,CompanyName,ContactName,ContactTitle,Country,PostalCode")] Customers customers)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                _dbContext.Customers.Update(customers);
                _dbContext.SaveChanges();

                return RedirectToAction("Customer");
            }
            return View(customers);
        }

        public IActionResult Delete(string id)
        {
            var cust = _dbContext.Customers.FirstOrDefault(m => m.CustomerId == id);
            if (cust == null)
            {
                return RedirectToAction("Customer");
            }
            _dbContext.Customers.Remove(cust);
            _dbContext.SaveChanges();
            return RedirectToAction("Customer");
        }

        public IActionResult Error()
        {
            return View();
        }
    }

}
```

## Create Views
1. Create Views/_ViewImports.cshtml
```
@using _50011210005
@addTagHelper *, Microsoft.AspNetCore.Mvc.TagHelpers

@{
    Layout = "_Layout";
}
```
2. Create Views/_ViewStart.cshtml
```
@{
    Layout = "_Layout";
}
```

3. Remove Tag Layout = "_Layout"; from your view page. becuese we use _ViewStart.cshtml.

4. Create file
```
 /Views/Manage/Customer.cshtml
 /Views/Manage/Create.cshtml 
 /Views/Manage/Edit.cshtml 

```
### Customer.cshtml
```
@model IEnumerable<_50011210005.Models.Customers>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
    
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"
            <br/>
              <a href="~/Manage/Create">Create New Customer</a>
              <br/>
              <h4>Persons List</h4>>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Customerid</th>
                  <th>Companyname</th>
                  <th>Contactname</th>
                  <th>Contacttitle</th>
                  <th>Country</th>
                  <th>Postalcode</th>
                </tr>
                </thead>
                <tbody>
                @foreach(var item in Model)
                {
                <tr>
                  <td>@item.CustomerId</td>
                  <td>@item.CompanyName</td>
                  <td>@item.ContactName</td>
                  <td>@item.ContactTitle</td>
                  <td>@item.Country</td>
                  <td>@item.PostalCode</td>
                  <td><a asp-action="Edit" asp-route-id="@item.CustomerId">Edit</a> |
                  <a asp-action="Delete" asp-route-id="@item.CustomerId">Delete</a></td>
                </tr>
                }
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
```

### Create.cshtml
```
@model _50011210005.Models.Customers
 
 <!-- Helper Tags -->
<form asp-controller="Manage" asp-action="Create"  method="post" class="form-horizontal">
    <h4>Create a new Person.</h4>
    <hr />
    <!--div asp-validation-summary="All" class="text-danger"></div -->
    <div class="form-group">
        <label asp-for="CompanyName" class="col-md-2 control-label"></label>
        <div class="col-md-10">
            <input asp-for="CompanyName" class="form-control" />
            <!-- span asp-validation-for="CompanyName" class="text-danger"></span -->
        </div>
    </div>
    <div class="form-group">
        <label asp-for="ContactName" class="col-md-2 control-label"></label>
        <div class="col-md-10">
            <input asp-for="ContactName" class="form-control" />
            <!-- span asp-validation-for="ContactName" class="text-danger"></span -->
        </div>
    </div>
     <div class="form-group">
        <label asp-for="Phone" class="col-md-2 control-label"></label>
        <div class="col-md-10">
            <input asp-for="ContactTitle" class="form-control" />
            <!-- span asp-validation-for="ContactTitle" class="text-danger"></span-->
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button type="submit" class="btn btn-default">Create</button>
        </div>
    </div>
</form>
 
<div>
    @Html.ActionLink("Back to List", "Customer")
</div>
```
### Edit.cshtml
```
@model _50011210005.Models.Customers
<h2>Edit</h2>
 
 <!-- Normal Razor Tags Helper -->

@using (Html.BeginForm()) {
    @Html.ValidationSummary(true)
 
    <fieldset>
        <legend>Person</legend>
 
        @Html.HiddenFor(model => model.CustomerId)
 
        <div class="editor-label">
            @Html.LabelFor(model => model.CompanyName)
        </div>
        <div class="editor-field">
            @Html.EditorFor(model => model.CompanyName)
        </div>
        <div class="editor-field">
            @Html.EditorFor(model => model.ContactName)
            @Html.ValidationMessageFor(model => model.ContactName)
        </div>
 
        <div class="editor-label">
            @Html.LabelFor(model => model.Country)
        </div>
        <div class="editor-field">
            @Html.EditorFor(model => model.Country)
            @Html.ValidationMessageFor(model => model.Country)
        </div>
 
        <div class="editor-label">
            @Html.LabelFor(model => model.PostalCode)
        </div>
        <div class="editor-field">
            @Html.EditorFor(model => model.PostalCode)
            @Html.ValidationMessageFor(model => model.PostalCode)
        </div>
        <br/>
        <div>
            <p>
                <input type="submit"  class="btn btn-default" value="Save" />
            </p>
        </div>
    </fieldset>
}
 
<div>
    @Html.ActionLink("Back to List", "Customer")
</div>
```
